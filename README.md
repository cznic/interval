# interval
Package interval handles sets of ordered values laying between two, possibly infinite, bounds.

Installation

    $ go get modernc.org/interval

Documentation: [godoc.org/modernc.org/interval](http://godoc.org/modernc.org/interval)
